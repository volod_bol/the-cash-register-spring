-- USERS
-- commented queries include decrypted password for authorization

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'volod', 'xs77W4', 'SENIORCASHIER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'volod', '$2a$10$FFlbm0dOcTkOakzG3edXQOSebkrbjfrIzugX9vZfOUpARp9U5ypsC', 'SENIORCASHIER');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'galiya', '483652fF', 'CASHIER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'galiya', '$2a$10$jfYmpoGSfZAdfsm/tScHCelpKXD0qYRSIYuy5jX.8JX5ZxiXGiO5O', 'CASHIER');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'oleksandrivna', 'gFjsk77', 'CASHIER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'oleksandrivna', '$2a$10$TcMTEFxL/w5EHHiH0Cob4.0/L/QJ2CCRMrGHABmKZh2ke6UOaOuSK', 'CASHIER');

-- INSERT INTO users (id, login, password, role) VALUES 
-- (DEFAULT, 'valentina', 'xcnGG24', 'STOREKEEPER');
INSERT INTO users (id, login, password, role) VALUES 
(DEFAULT, 'valentina', '$2a$10$G9RMPw1Yhq3bLaNZrBl9DO2SWJ2Y8Xfs99iK8TFmgP70/HBts5o9C', 'STOREKEEPER');

-- List of goods
INSERT INTO goods (id, title, quantity) VALUES 
(DEFAULT, 'Кола 1.5', 100),
(DEFAULT, 'Газована вода 1.5', 100),
(DEFAULT, 'Хліб', 25),
(DEFAULT, 'Булочка з маком', 57),
(DEFAULT, 'Пачки масла', 43),
(DEFAULT, 'Снікерс', 87),
(DEFAULT, 'Баунті', 143),
(DEFAULT, 'Піца', 5),
(DEFAULT, 'Антисептик', 366),
(DEFAULT, 'Маска медична', 12225),
(DEFAULT, 'Плейстешн 5', 6322),
(DEFAULT, 'Упаковка води 5л', 100),
(DEFAULT, 'Макбук М1', 2),
(DEFAULT, 'Солодощі', 3563),
(DEFAULT, 'Папір', 8796),
(DEFAULT, 'Портативна колонка', 345),
(DEFAULT, 'Монітор', 1235),
(DEFAULT, 'Ручка', 135),
(DEFAULT, 'Фонарик', 123),
(DEFAULT, 'Телефон Fly', 47),
(DEFAULT, 'Натс', 216),
(DEFAULT, 'Абонемент в спортзал', 125),
(DEFAULT, 'Компютерна миша', 366),
(DEFAULT, 'Кабель Type-C', 5212),
(DEFAULT, 'Айфон 4', 2435),
(DEFAULT, 'Сяомі Нот Супер Ультра 10 Макс', 439),
(DEFAULT, 'Пральна машина LG', 854),
(DEFAULT, 'Чіпси Lays', 6),
(DEFAULT, 'Морозиво', 55),
(DEFAULT, 'Банани', 45.75),
(DEFAULT, 'Куряча гомілка', 89.5),
(DEFAULT, 'Мюслі', 56),
(DEFAULT, 'RTX 3090', 234),
(DEFAULT, 'Плейсейшен 4', 2),
(DEFAULT, 'Останній XBOX', 80),
(DEFAULT, 'Lenovo IDEAPad 300x 45-etfjr-7', 4);