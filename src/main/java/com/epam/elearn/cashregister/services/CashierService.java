package com.epam.elearn.cashregister.services;

import com.epam.elearn.cashregister.entity.Order;
import com.epam.elearn.cashregister.entity.OrderGoods;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.GoodsRepository;
import com.epam.elearn.cashregister.repository.OrderRepository;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
public class CashierService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CashierService.class);

    private static final String ORDER_GOODS_LIST = "orderGoodsList";
    private static final String LAST_ADDED_GOODS = "lastAddedGoods";
    private static final String INCORRECT_ORDER_INPUT = "incorrectOrderInput";

    private final GoodsRepository goodsRepository;
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;

    @Autowired
    public CashierService(GoodsRepository goodsRepository,
                          OrderRepository orderRepository,
                          UserRepository userRepository) {
        this.goodsRepository = goodsRepository;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
    }

    /**
     * Add new goods to order. If order already has that goods, method just adds old and new goods amount
     *
     * @param orderGoodsList - order list that contain goods and their amount
     * @param item           - new goods that need add to order (check)
     */
    public void addGoodsToList(List<OrderGoods> orderGoodsList, OrderGoods item) {
        orderGoodsList.stream()
                .filter(orderGoods -> orderGoods.getGoods().getTitle().equals(item.getGoods().getTitle()))
                .findFirst()
                .ifPresentOrElse(
                        orderGoods -> {
                            double currAmount = orderGoods.getAmount();
                            double newAmount = item.getAmount();
                            orderGoods.setAmount(currAmount + newAmount);
                        }, () -> orderGoodsList.add(item)
                );
    }

    /**
     * Adds goods to order list by id. Order goods object contain goods object (with quantity on warehouse) and amount in check.
     *
     * @param goodsIdentifierInput ID of goods
     * @param goodsAmount          amount of goods
     * @param session              session that contain order (list with goods and their amount)
     */
    public void addGoodsToOrderById(String goodsIdentifierInput, String goodsAmount, HttpSession session) {
        List<OrderGoods> orderGoodsList = (List<OrderGoods>) session.getAttribute(ORDER_GOODS_LIST);

        OrderGoods orderGoods = new OrderGoods();
        goodsAmount = goodsAmount.replace(",", ".");
        orderGoods.setAmount(Double.parseDouble(goodsAmount));

        goodsRepository.findById(Integer.parseInt(goodsIdentifierInput))
                .ifPresentOrElse(
                        goods -> {
                            orderGoods.setGoods(goods);
                            session.setAttribute(LAST_ADDED_GOODS, orderGoods);
                            addGoodsToList(orderGoodsList, orderGoods);
                            LOGGER.info(String.format("Added new goods to order, id: %s, title: %s, amount: %s", goodsIdentifierInput, goods.getTitle(), orderGoods.getAmount()));
                        },
                        () -> {
                            session.setAttribute(INCORRECT_ORDER_INPUT, true);
                            LOGGER.error(String.format("Cannot add goods to order, id: %s", goodsIdentifierInput));
                        });
    }

    /**
     * Adds goods to order list by title. Order goods object contain goods object (with quantity on warehouse) and amount in check.
     *
     * @param goodsIdentifierInput goods title
     * @param goodsAmount          amount of goods
     * @param session              session that contain order (list with goods and their amount)
     */
    public void addGoodsToOrderByTitle(String goodsIdentifierInput, String goodsAmount, HttpSession session) {
        List<OrderGoods> orderGoodsList = (List<OrderGoods>) session.getAttribute(ORDER_GOODS_LIST);
        OrderGoods orderGoods = new OrderGoods();
        goodsAmount = goodsAmount.replace(",", ".");
        orderGoods.setAmount(Double.parseDouble(goodsAmount));

        goodsRepository.findByTitle(goodsIdentifierInput)
                .ifPresentOrElse(goods -> {
                    orderGoods.setGoods(goods);
                    session.setAttribute(LAST_ADDED_GOODS, orderGoods);
                    addGoodsToList(orderGoodsList, orderGoods);
                    LOGGER.info(String.format("Added new goods to order, id: %s, title: %s, amount: %s", goods.getId(), goods.getTitle(), orderGoods.getAmount()));
                }, () -> {
                    session.setAttribute(INCORRECT_ORDER_INPUT, true);
                    LOGGER.error(String.format("Cannot add goods to order, title: %s", goodsIdentifierInput));
                });
    }

    /**
     * Confirm order. Order it`s list with goods and their amount in current check.
     *
     * @param orderGoodsList list with goods
     * @param session        session that contain order list
     * @param principal      spring mvc object that contain logged user name
     */
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void confirmOrder(List<OrderGoods> orderGoodsList, HttpSession session, Principal principal) {
        Optional<User> user = userRepository.findUserByLogin(principal.getName());
        if (user.isPresent()) {
            Order order = new Order(user.get(), LocalDate.now(), LocalTime.now(), orderGoodsList);
            orderRepository.save(order);
            session.removeAttribute(ORDER_GOODS_LIST);
        }
    }

}
