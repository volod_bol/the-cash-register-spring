package com.epam.elearn.cashregister.services;

import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

@Service
public class RegistrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationService.class);

    private static final String SUCCESS_REGISTRATION = "successRegistration";
    private static final String USER_LOGIN = "userLogin";
    private static final String USER_PASSWORD = "userPassword";
    private static final String USER_ROLE = "userRole";

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Autowired
    public RegistrationService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    /**
     * Register new user and transfer user login, password and role to session that show it on page later
     *
     * @param session      user session
     * @param userLogin    user login
     * @param userPassword user password
     * @param userRole     user role
     */
    @Transactional
    public void registerNewUser(HttpSession session,
                                String userLogin,
                                String userPassword,
                                String userRole) {
        String encodedPassword = passwordEncoder.encode(userPassword.subSequence(0, userPassword.length()));
        User user = new User(userLogin, encodedPassword, userRole);
        userRepository.save(user);
        session.setAttribute(SUCCESS_REGISTRATION, true);
        session.setAttribute(USER_LOGIN, userLogin);
        session.setAttribute(USER_PASSWORD, userPassword);
        session.setAttribute(USER_ROLE, userRole);
        session.setAttribute("currentlyRegistered", true);
        LOGGER.info("New user in system. Id: {}, login: {}, role: {}", user.getId(), user.getLogin(), user.getRole());
    }
}
