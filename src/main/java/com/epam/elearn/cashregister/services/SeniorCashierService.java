package com.epam.elearn.cashregister.services;

import com.epam.elearn.cashregister.entity.Order;
import com.epam.elearn.cashregister.entity.OrderGoods;
import com.epam.elearn.cashregister.entity.Report;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.OrderRepository;
import com.epam.elearn.cashregister.repository.ReportRepository;
import com.epam.elearn.cashregister.repository.UserRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SeniorCashierService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeniorCashierService.class);

    private static final String SUCCESS_REGISTRATION = "successRegistration";
    private static final String USER_LOGIN = "userLogin";
    private static final String USER_PASSWORD = "userPassword";
    private static final String USER_ROLE = "userRole";

    private static final String DATE_TIME_TABULATION = "%-45s %s";
    private static final String NUMBER_TABULATION = "%-52s %s";

    private final ReportRepository reportRepository;
    private final OrderRepository orderRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Autowired
    public SeniorCashierService(ReportRepository reportRepository,
                                OrderRepository orderRepository,
                                @Qualifier("passwordEncoder") PasswordEncoder passwordEncoder,
                                UserRepository userRepository) {
        this.reportRepository = reportRepository;
        this.orderRepository = orderRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    /**
     * Delete selected order
     *
     * @param id order id
     */
    @Transactional
    public void deleteOrder(int id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            orderRepository.delete(optionalOrder.get());
            LOGGER.info("Order was deleted, order id: {}", id);
        } else {
            LOGGER.error("Order was not deleted, order id: {}", id);
        }
    }

    /**
     * Delete goods from check (order). If order exist method look up for goods (by id) and delete it.
     * If order don`t has any goods, it will be deleted.
     *
     * @param orderId order id
     * @param goodsId goods id
     */
    @Transactional
    public void deleteGoodsInOrder(int orderId, int goodsId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            List<OrderGoods> orderGoodsList = order.getOrderGoodsList().stream()
                    .filter(orderGoods -> orderGoods.getGoods().getId() != goodsId)
                    .collect(Collectors.toList());
            order.setOrderGoodsList(orderGoodsList);
            if (orderGoodsList.isEmpty()) {
                orderRepository.delete(order);
            } else {
                orderRepository.save(order);
            }
            LOGGER.info("Goods in order was deleted, order id: {}, goods id: {}", orderId, goodsId);
        }
    }

    /**
     * Register new user and transfer user login, password and role to session that show it on page later
     *
     * @param session      user session
     * @param userLogin    user login
     * @param userPassword user password
     * @param userRole     user role
     */
    @Transactional
    public void registerNewUser(HttpSession session,
                                String userLogin,
                                String userPassword,
                                String userRole) {
        String encodedPassword = passwordEncoder.encode(userPassword.subSequence(0, userPassword.length()));
        User user = new User(userLogin, encodedPassword, userRole);
        userRepository.save(user);
        session.setAttribute(SUCCESS_REGISTRATION, true);
        session.setAttribute(USER_LOGIN, userLogin);
        session.setAttribute(USER_PASSWORD, userPassword);
        session.setAttribute(USER_ROLE, userRole);
        LOGGER.info("New user in system. Id: {}, login: {}, role: {}", user.getId(), user.getLogin(), user.getRole());
    }

    /**
     * Create report with full information and save information about creating report in database.
     * X report just give all information, when Z report do the same, but also begin new trade shift (without old checks).
     *
     * @param user        current user that create report
     * @param reportType  type of report
     * @param orders      order list of trade shift
     * @param lastOrderId id of last order while report creates
     * @return byte array output stream
     */
    public ByteArrayOutputStream createReport(User user, String reportType, List<Order> orders, long lastOrderId) {
        Document document = new Document(PageSize.A8, 3, 3, 5, 5);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();
            BaseFont baseFont = BaseFont.createFont("NotoSansMonoRegular.ttf", "cp1251", BaseFont.EMBEDDED);
            Font font = new Font(baseFont, 4);
            Paragraph border = new Paragraph("----------------------------------------------------------", font);
            Paragraph delimiter = new Paragraph("==========================================================", font);

            int purchasedGoods = orders.stream()
                    .map(order -> order.getOrderGoodsList().size())
                    .reduce(0, (acc, countOfGoods) -> acc += countOfGoods);
            int countOfOrders = orders.size();

            Report report = reportRepository.save(new Report(lastOrderId, reportType, user, LocalDate.now(), LocalTime.now()));
            int reportId = report.getId();

            Paragraph companyTitle = new Paragraph("ТОВ \"МАНІЯ-ДРАЙВ\"", font);
            Paragraph location = new Paragraph("Київська обл.,Бориспільский р-н,с.Щасливе", font);
            Paragraph council = new Paragraph("с/р Щаслива", font);
            Paragraph street = new Paragraph("вул.Григорія Сковороди,21,заїзд зліва", font);
            Paragraph taxNumber = new Paragraph("ПН 312334014299", font);
            companyTitle.setAlignment(Element.ALIGN_CENTER);
            location.setAlignment(Element.ALIGN_CENTER);
            council.setAlignment(Element.ALIGN_CENTER);
            street.setAlignment(Element.ALIGN_CENTER);
            taxNumber.setAlignment(Element.ALIGN_CENTER);

            document.add(companyTitle);
            document.add(location);
            document.add(council);
            document.add(street);
            document.add(taxNumber);

            document.add(border);
            Paragraph reportTitle;
            if (reportType.equals("Z")) {
                reportTitle = new Paragraph("Z звіт № " + reportId, font);
            } else {
                reportTitle = new Paragraph("X звіт № " + reportId, font);
            }
            reportTitle.setAlignment(Element.ALIGN_CENTER);
            document.add(reportTitle);
            document.add(border);

            LocalDate date = LocalDate.now();
            Paragraph dateGetting = new Paragraph(String.format(DATE_TIME_TABULATION, "Дата програмування:",
                    date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))), font);
            document.add(dateGetting);
            document.add(delimiter);

            Paragraph ordersParagraph = new Paragraph("ЗАМОВЛЕННЯ", font);
            ordersParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(ordersParagraph);

            writeOrdersInReport(orders, document, font, border, delimiter);

            document.add(delimiter);
            Paragraph amountOfPurchasedGoods = new Paragraph(String.format(NUMBER_TABULATION, "Кількість проданих товарів:", purchasedGoods), font);
            document.add(amountOfPurchasedGoods);
            Paragraph amountOfOrders = new Paragraph(String.format(NUMBER_TABULATION, "Кількість чеків:", countOfOrders), font);
            document.add(amountOfOrders);
            document.add(border);

            document.close();
            writer.close();

        } catch (IOException | DocumentException e) {
            LOGGER.error("Error during generation report, report type: {}", reportType);
            LOGGER.error("Report error info", e);
        }
        return outputStream;
    }

    /**
     * Uses in {@link SeniorCashierService#createReport(User, String, List, long)} method. Just put all orders in report.
     *
     * @param orders    list of orders
     * @param document  pdf document of report
     * @param font      current font
     * @param border    border for list of orders
     * @param delimiter delimiter for list of orders
     * @throws DocumentException exception
     */
    private void writeOrdersInReport(List<Order> orders, Document document, Font font, Paragraph border, Paragraph delimiter) throws DocumentException {
        for (Order order : orders) {
            document.add(delimiter);
            Paragraph id = new Paragraph(String.format("%-28s %s", "ID замовлення:", order.getId()), font);
            document.add(id);

            Paragraph userParagraph = new Paragraph("Касир: " + order.getUser().getLogin(), font);
            document.add(userParagraph);

            Paragraph orderDate = new Paragraph(String.format(DATE_TIME_TABULATION, "Дата видачі:",
                    order.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))), font);
            document.add(orderDate);

            Paragraph orderTime = new Paragraph(String.format(DATE_TIME_TABULATION, "Час видачі:",
                    order.getTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))), font);
            document.add(orderTime);

            Paragraph goodsParagraph = new Paragraph("ТОВАРИ ЗАМОВЛЕННЯ", font);
            goodsParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(goodsParagraph);

            List<OrderGoods> goodsList = order.getOrderGoodsList();
            for (OrderGoods goods : goodsList) {
                document.add(border);
                Paragraph goodsId = new Paragraph(String.format(NUMBER_TABULATION, "ID:", goods.getGoods().getId()), font);
                document.add(goodsId);
                Paragraph goodsTitle = new Paragraph(String.format("%s %s", "Назва:", goods.getGoods().getTitle()), font);
                document.add(goodsTitle);
                Paragraph goodsAmount = new Paragraph(String.format(NUMBER_TABULATION, "Кількість:", goods.getAmount()), font);
                document.add(goodsAmount);
            }
        }
    }

}
