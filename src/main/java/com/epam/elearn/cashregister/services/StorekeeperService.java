package com.epam.elearn.cashregister.services;

import com.epam.elearn.cashregister.entity.Goods;
import com.epam.elearn.cashregister.repository.GoodsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StorekeeperService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorekeeperService.class);

    private final GoodsRepository goodsRepository;

    @Autowired
    public StorekeeperService(GoodsRepository goodsRepository) {
        this.goodsRepository = goodsRepository;
    }

    /**
     * Change quantity of goods, if user write wrong number or value via "," shows error on page
     *
     * @param goods         current goods
     */
    public void changeQuantity(Goods goods) {
        goodsRepository.save(goods);
        LOGGER.info("Quantity of goods was changed, goods title: {}, new quantity: {}", goods.getTitle(), goods.getQuantity());
    }

    /**
     * Add new goods in database.
     * @param goods
     */
    public void addNewGoods(Goods goods) {
        goodsRepository.save(goods);
        LOGGER.info("Was added new goods, title: {}, quantity: {}", goods.getTitle(), goods.getQuantity());
    }
}
