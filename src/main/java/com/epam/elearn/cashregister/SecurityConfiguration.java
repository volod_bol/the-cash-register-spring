package com.epam.elearn.cashregister;

import com.epam.elearn.cashregister.auth.ApplicationUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.concurrent.TimeUnit;

import static com.epam.elearn.cashregister.entity.Role.*;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final ApplicationUserDetailsService applicationUserDetailsService;

    @Autowired
    public SecurityConfiguration(ApplicationUserDetailsService userService) {
        this.applicationUserDetailsService = userService;
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    /**
     * Access to /cashier/**, /seniorcashier/**, /storekeeper/** pages has user only with proper role.
     * Login page accessible for everyone. Remember me token valid 14 day. Logout invalidate session and delete
     * cookies that related with session and remember me token`s.
     *
     * @param http HttpSecurity
     * @throws Exception exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/cashier/**").hasRole(CASHIER.name())
                    .antMatchers("/seniorcashier/**").hasRole(SENIORCASHIER.name())
                    .antMatchers("/storekeeper/**").hasRole(STOREKEEPER.name())
                .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll(true)
                    .usernameParameter("login")
                    .defaultSuccessUrl("/login/redirect", true)
                .and()
                    .rememberMe()
                    .tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(14))
                    .rememberMeParameter("remember-me")
                .and()
                    .logout()
                    .logoutSuccessUrl("/login")
                    .clearAuthentication(true)
                    .invalidateHttpSession(true)
                    .deleteCookies("JSESSIONID", "remember-me");
    }

    /**
     * Bean that Spring Security uses for user authentication. We set password encoder and user detail service
     * for dao provider.
     *
     * @return authentication provider for security
     */
    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setPasswordEncoder(getPasswordEncoder());
        provider.setUserDetailsService(applicationUserDetailsService);
        return provider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }
}
