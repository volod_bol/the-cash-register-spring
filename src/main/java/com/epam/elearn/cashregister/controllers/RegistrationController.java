package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.repository.UserRepository;
import com.epam.elearn.cashregister.services.RegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class RegistrationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    private static final String INCORRECT_LOGIN = "incorrectLogin";
    private static final String LOGIN_INPUT = "loginInput";

    private final UserRepository userRepository;
    private final RegistrationService registrationService;

    @Autowired
    public RegistrationController(UserRepository userRepository, RegistrationService registrationService) {
        this.userRepository = userRepository;
        this.registrationService = registrationService;
    }

    @GetMapping("/sign-up")
    public String registerUserPage(HttpSession session,
                                   Model model) {
        markIfWrongInput(INCORRECT_LOGIN, session, model);
        markIfWrongInput("isLoginUsed", session, model);
        markIfWrongInput("incorrectPassword", session, model);
        markIfWrongInput("incorrectRepeatedPassword", session, model);
        passLoginOnPage(session, model);
        return "registration";
    }

    /**
     * Check if input correct and if so save new user in database. If not show error on page.
     *
     * @param session              user session
     * @param userLogin            new user login
     * @param userPassword         new user password
     * @param repeatedUserPassword new user repeated password
     * @param userRole             new user role in system
     * @return url for redirecting
     */
    @PostMapping("/sign-up")
    public String registerNewUser(HttpSession session,
                                  @RequestParam(name = "userLogin") String userLogin,
                                  @RequestParam(name = "userPassword") String userPassword,
                                  @RequestParam(name = "repeatedUserPassword") String repeatedUserPassword,
                                  @RequestParam(name = "userRole") String userRole) {
        boolean isAllInputCorrect = isNewUserInputCorrect(userLogin, userPassword, repeatedUserPassword, session);
        if (isAllInputCorrect) {
            registrationService.registerNewUser(session, userLogin, userPassword, userRole);
            return "redirect:/login";
        } else {
            return "redirect:/sign-up";
        }
    }

    /**
     * If username already exist return false. If login do not match regex return false. If password do not match regex
     * or repeated password do not equal other password return false.
     *
     * @param userLogin            new user login
     * @param userPassword         new user password
     * @param repeatedUserPassword new user repeated password
     * @param session              user session
     * @return true or false depending on rules above
     */
    public boolean isNewUserInputCorrect(String userLogin, String userPassword, String repeatedUserPassword, HttpSession session) {
        boolean isInputCorrect = isLoginFree(userLogin, session);
        if (isInputCorrect && isLoginCorrect(userLogin)) {
            session.setAttribute(LOGIN_INPUT, userLogin);
            LOGGER.info("Login correct, login: {}", userLogin);
        } else {
            isInputCorrect = false;
            session.setAttribute(INCORRECT_LOGIN, true);
            LOGGER.error("Cannot register new user, wrong login, or already used: {}", userLogin);
        }
        if (!isPasswordCorrect(userPassword)) {
            isInputCorrect = false;
            session.setAttribute("incorrectPassword", true);
            LOGGER.error("Cannot register new user, wrong password: {}", userPassword);
        }
        if (!userPassword.equals(repeatedUserPassword)) {
            isInputCorrect = false;
            session.setAttribute("incorrectRepeatedPassword", true);
            LOGGER.error("Cannot register new user, password and repeated password are different, password: {}, repeated password: {}", userPassword, repeatedUserPassword);
        }
        return isInputCorrect;
    }

    /**
     * If session contains information about wrong input method put it in model and remove from session.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void markIfWrongInput(String errorCode, HttpSession session, Model model) {
        Object incorrectInput = session.getAttribute(errorCode);
        if (incorrectInput != null) {
            model.addAttribute(errorCode, incorrectInput);
            session.removeAttribute(errorCode);
        }
    }

    /**
     * If new user login correct but other not, pass in login page that login.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void passLoginOnPage(HttpSession session, Model model) {
        String userLogin = (String) session.getAttribute(LOGIN_INPUT);
        if (userLogin != null) {
            model.addAttribute(LOGIN_INPUT, userLogin);
            session.removeAttribute(LOGIN_INPUT);
        }
    }

    /**
     * If login does not blank and match regex return true.
     *
     * @param login new user login
     * @return true or false depending on rule above
     */
    boolean isLoginCorrect(String login) {
        return !login.isBlank() && login.matches("[a-z0-9_-]{5,16}");
    }

    /**
     * If password does not blank and match regex return true.
     *
     * @param password new user password
     * @return true or false depending on rule above
     */
    boolean isPasswordCorrect(String password) {
        return !password.isBlank() && password.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");
    }

    /**
     * Check if new user login is already exist.
     *
     * @param userLogin new user login
     * @param session   user session
     * @return true or false depending on rule above
     */
    boolean isLoginFree(String userLogin, HttpSession session) {
        boolean isLoginUsed = userRepository.findUserByLogin(userLogin).isPresent();
        if (isLoginUsed) {
            session.setAttribute("isLoginUsed", true);
            LOGGER.error("Cannot register new user, login is exist: {}", userLogin);
            return false;
        }
        return true;
    }

}
