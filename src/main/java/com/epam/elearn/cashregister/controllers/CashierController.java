package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.entity.OrderGoods;
import com.epam.elearn.cashregister.repository.GoodsRepository;
import com.epam.elearn.cashregister.services.CashierService;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/cashier")
public class CashierController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CashierController.class);

    private static final String ORDER_GOODS_LIST = "orderGoodsList";
    private static final String LAST_ADDED_GOODS = "lastAddedGoods";
    private static final String INCORRECT_ORDER_INPUT = "incorrectOrderInput";

    private final GoodsRepository goodsRepository;
    private final CashierService cashierService;

    @Autowired
    public CashierController(GoodsRepository goodsRepository,
                             CashierService cashierService) {
        this.goodsRepository = goodsRepository;
        this.cashierService = cashierService;
    }

    @GetMapping(value = "/goods")
    public String goodsInStock(@RequestParam(value = "offset", required = false) Integer offset,
                               @RequestParam(value = "limitSelect", required = false) Integer newLimit,
                               HttpSession session,
                               Model model) {
        renderGoodsCashier(offset, newLimit, session, model);
        return "cashier/cashierGoods";
    }

    @GetMapping
    public String mainPage(HttpSession session,
                           Model model) {
        markIfWrongInput(session, model);
        setOrdersFromSessionToModel(session, model);
        setLastAddedGoodsToModel(session, model);
        return "cashier/cashierMain";
    }

    /**
     * Firstly check if user input is correct, if not show error on page and log it.
     * Then depends on goods identifier add goods to order.
     *
     * @param goodsIdentifier      goods identifier (id or title)
     * @param goodsIdentifierInput user input of identifier
     * @param goodsAmount          amount of ordered goods
     * @param session              session that contain order and additional information (such as isInputIncorrect and others)
     * @return url for redirecting
     */
    @PutMapping
    public String addGoodsToOrder(@RequestParam(value = "goodsIdentifier") String goodsIdentifier,
                                  @RequestParam(value = "goodsIdentifierInput") String goodsIdentifierInput,
                                  @RequestParam(value = "goodsAmount") String goodsAmount,
                                  HttpSession session) {
        boolean isInputCorrect = isOrderInputCorrect(goodsIdentifier, goodsIdentifierInput, goodsAmount);
        if (isInputCorrect) {
            if ("id".equals(goodsIdentifier)) {
                cashierService.addGoodsToOrderById(goodsIdentifierInput, goodsAmount, session);
            }
            cashierService.addGoodsToOrderByTitle(goodsIdentifierInput, goodsAmount, session);
        } else {
            session.setAttribute(INCORRECT_ORDER_INPUT, true);
            LOGGER.error("Cannot add goods to order, wrong input! Identifier: {}, identifier input: {}, amount: {}", goodsIdentifier, goodsIdentifierInput, goodsAmount);
        }
        return "redirect:/cashier";
    }

    @GetMapping("/review")
    public String reviewOrder(HttpSession session,
                              Model model) {
        markIfWrongInput(session, model);
        setOrdersFromSessionToModel(session, model);
        return "cashier/reviewOrder";
    }

    @GetMapping("/review/{id}/edit")
    public String editAmountInOrder(@PathVariable int id,
                                    @SessionAttribute("orderGoodsList") List<OrderGoods> orderGoodsList,
                                    Model model) {
        Optional<OrderGoods> goodsOptional = orderGoodsList.stream()
                .filter(orderGoods -> orderGoods.getGoods().getId() == id)
                .findFirst();
        goodsOptional.ifPresent(orderGoods -> model.addAttribute("goodsInOrder", orderGoods));
        return "cashier/editOrderGoods";
    }

    /**
     * Change goods amount on reviewing order page. Firstly check that user input correct. If not, show error
     * and anew add that goods in model for showing on page. If so, find out in order list that goods and replace old
     * amount on new.
     *
     * @param id             goods id
     * @param orderGoodsList list with goods in order
     * @param newGoodsAmount new amount of current goods
     * @param model          model for thymeleaf resolver
     * @return url for redirecting or again link on that page with error
     */
    @PatchMapping("/review/{id}")
    public String confirmEditingAmountInOrder(@PathVariable int id,
                                              @SessionAttribute("orderGoodsList") List<OrderGoods> orderGoodsList,
                                              @RequestParam(value = "newGoodsAmount") String newGoodsAmount,
                                              Model model) {
        if (isQuantityCorrect(newGoodsAmount)) {
            newGoodsAmount = newGoodsAmount.replace(",", ".");
            double newAmount = Double.parseDouble(newGoodsAmount);
            Optional<OrderGoods> goodsOptional = orderGoodsList.stream()
                    .filter(orderGoods -> orderGoods.getGoods().getId() == id)
                    .findFirst();
            goodsOptional.ifPresent(orderGoods -> orderGoods.setAmount(newAmount));
        } else {
            Optional<OrderGoods> goodsOptional = orderGoodsList.stream()
                    .filter(orderGoods -> orderGoods.getGoods().getId() == id)
                    .findFirst();
            goodsOptional.ifPresent(orderGoods -> model.addAttribute("goodsInOrder", orderGoods));
            model.addAttribute(INCORRECT_ORDER_INPUT, true);
            return "cashier/editOrderGoods";
        }
        return "redirect:/cashier/review";
    }

    /**
     * Confirm order and save it in database.
     *
     * @param orderGoodsList list of goods in order
     * @param session        user session
     * @param principal      simple entity from spring security that contain username
     * @return url for redirecting
     */
    @PostMapping
    public String confirmOrder(@SessionAttribute("orderGoodsList") List<OrderGoods> orderGoodsList,
                               HttpSession session,
                               Principal principal) {
        cashierService.confirmOrder(orderGoodsList, session, principal);
        return "redirect:/cashier";
    }

    /**
     * Its main method that responsible for pagination and rendering goods on page. Always check limit and current offset
     * for showing items.
     *
     * @param offset   current offset (default zero)
     * @param newLimit if user change limit on page it will be saved here
     * @param session  user session
     * @param model    model for thymeleaf resolver
     */
    private void renderGoodsCashier(Integer offset, Integer newLimit, HttpSession session, Model model) {
        int limit = 5;
        if (newLimit != null) {
            limit = newLimit;
            offset = 0;
            session.setAttribute("lastLimit", limit);
        } else {
            Object lastLimit = session.getAttribute("lastLimit");
            if (lastLimit != null) {
                limit = (Integer) lastLimit;
            }
        }

        offset = setOffsetFromSession(offset, session);

        long pages = 0;
        if (goodsRepository.count() != limit) {
            pages = Math.round(goodsRepository.count() / (double) limit);
        }

        model.addAttribute("goods", goodsRepository.findAllPagination(limit, offset));
        model.addAttribute("pages", pages);
        model.addAttribute("goodsCount", goodsRepository.count());
        model.addAttribute("offset", offset);
        long currentPage = Math.round(offset / (double) limit);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("limit", limit);
    }

    /**
     * Get last offset from session, default offset is zero.
     *
     * @param offset  offset
     * @param session user session
     * @return last known offset
     */
    private Integer setOffsetFromSession(Integer offset, HttpSession session) {
        if (offset == null) {
            Object lastOffset = session.getAttribute("lastOffset");
            if (lastOffset != null) {
                offset = (Integer) lastOffset;
            } else {
                offset = 0;
            }
        }
        session.setAttribute("lastOffset", offset);
        return offset;
    }

    /**
     * Get order list from session and put it in model.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void setOrdersFromSessionToModel(HttpSession session, Model model) {
        List<OrderGoods> goodsOrderList = (List<OrderGoods>) session.getAttribute(ORDER_GOODS_LIST);
        if (goodsOrderList != null) {
            model.addAttribute(ORDER_GOODS_LIST, goodsOrderList);
        } else {
            goodsOrderList = new ArrayList<>();
            session.setAttribute(ORDER_GOODS_LIST, goodsOrderList);
            model.addAttribute(ORDER_GOODS_LIST, goodsOrderList);
        }
    }

    /**
     * If session contains information about wrong input method put it in model and remove from session.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void markIfWrongInput(HttpSession session, Model model) {
        Object incorrectInput = session.getAttribute(INCORRECT_ORDER_INPUT);
        if (incorrectInput != null) {
            model.addAttribute(INCORRECT_ORDER_INPUT, incorrectInput);
            session.removeAttribute(INCORRECT_ORDER_INPUT);
        }
    }

    /**
     * Get information about last added goods in order and show it on page.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void setLastAddedGoodsToModel(HttpSession session, Model model) {
        Object attribute = session.getAttribute(LAST_ADDED_GOODS);
        if (attribute != null) {
            OrderGoods orderGoods = (OrderGoods) session.getAttribute(LAST_ADDED_GOODS);
            model.addAttribute("lastGoodsId", orderGoods.getGoods().getId());
            model.addAttribute("lastGoodsTitle", orderGoods.getGoods().getTitle());
            model.addAttribute("lastGoodsAmount", orderGoods.getAmount());
            session.removeAttribute(LAST_ADDED_GOODS);
        }
    }

    /**
     * Check that user input on order page for adding goods is correct.
     *
     * @param identifier      goods identifier
     * @param identifierInput user input of identifier
     * @param amount          goods amount in receipt
     * @return the result of all inspections
     */
    boolean isOrderInputCorrect(String identifier, String identifierInput, String amount) {
        if (identifier.equals("id") && !isIdInputCorrect(identifierInput)) {
            return false;
        }
        if (!isQuantityCorrect(amount))
            return false;
        return !identifierInput.isBlank();
    }

    /**
     * Check that id input is correct.
     *
     * @param idInput id input
     * @return true if id greater than zero. False if its text or something else that not id.
     */
    private boolean isIdInputCorrect(String idInput) {
        try {
            return Integer.parseInt(idInput) > 0;
        } catch (NumberFormatException e) {
            LOGGER.error("Wrong id input: {}", idInput);
            return false;
        }
    }

    /**
     * Check that quantity is correct.
     *
     * @param number quantity
     * @return true if amount greater than zero, and false if number its text or something else.
     */
    boolean isQuantityCorrect(String number) {
        number = number.replace(",", ".");
        if (NumberUtils.isParsable(number)) {
            double goodsAmount = Double.parseDouble(number);
            return goodsAmount > 0;
        } else {
            return false;
        }
    }

}
