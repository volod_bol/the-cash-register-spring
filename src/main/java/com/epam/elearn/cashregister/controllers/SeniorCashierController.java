package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.entity.Order;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.OrderRepository;
import com.epam.elearn.cashregister.repository.ReportRepository;
import com.epam.elearn.cashregister.repository.UserRepository;
import com.epam.elearn.cashregister.services.SeniorCashierService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/seniorcashier")
public class SeniorCashierController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SeniorCashierController.class);

    private static final String INCORRECT_LOGIN = "incorrectLogin";
    private static final String SUCCESS_REGISTRATION = "successRegistration";
    private static final String USER_LOGIN = "userLogin";
    private static final String USER_PASSWORD = "userPassword";
    private static final String LOGIN_INPUT = "loginInput";
    private static final String USER_ROLE = "userRole";

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final ReportRepository reportRepository;
    private final SeniorCashierService seniorCashierService;
    private final SessionFactory sessionFactory;

    @Autowired
    public SeniorCashierController(OrderRepository orderRepository,
                                   UserRepository userRepository,
                                   ReportRepository reportRepository,
                                   EntityManagerFactory factory,
                                   SeniorCashierService seniorCashierService) {
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.reportRepository = reportRepository;
        this.sessionFactory = factory.unwrap(SessionFactory.class);
        this.seniorCashierService = seniorCashierService;
    }

    @GetMapping
    public String mainPage(@RequestParam(value = "offset", required = false) Integer offset,
                           @RequestParam(value = "limitSelect", required = false) Integer newLimit,
                           HttpSession session,
                           Model model) {
        renderOrders(offset, newLimit, session, model);
        return "seniorcashier/seniorcashierMain";
    }

    /**
     * Its main method that responsible for pagination and rendering orders on page. Always check limit and current offset
     * for showing items.
     *
     * @param offset   current offset (default zero)
     * @param newLimit if user change limit on page it will be saved here
     * @param session  user session
     * @param model    model for thymeleaf resolver
     */
    private void renderOrders(Integer offset, Integer newLimit, HttpSession session, Model model) {
        int limit = 5;
        if (newLimit != null) {
            limit = newLimit;
            offset = 0;
            session.setAttribute("lastLimit", limit);
        } else {
            Object lastLimit = session.getAttribute("lastLimit");
            if (lastLimit != null) {
                limit = (Integer) lastLimit;
            }
        }

        offset = setOffsetFromSession(offset, session);

        List<Order> resultList = getOrdersByPagination(offset, limit, model);

        model.addAttribute("orders", resultList);
        model.addAttribute("offset", offset);
        long currentPage = Math.round(offset / (double) limit);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("limit", limit);
    }

    /**
     * Open session and create a query with offset and limit. Also order it by descending id. Math number of pages for
     * pagination and put it in model.
     *
     * @param offset current offset
     * @param limit  limit items on page
     * @param model  model for thymeleaf resolver
     * @return list with wanted orders
     */
    private List<Order> getOrdersByPagination(Integer offset, int limit, Model model) {
        List<Order> resultList;
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);
            Root<Order> orderRoot = criteriaQuery.from(Order.class);

            Integer lastOrderIdFromReport = 0;
            Optional<Integer> optionalLastOrderId = reportRepository.getLastOrderIdFromReport();
            if (optionalLastOrderId.isPresent()) {
                lastOrderIdFromReport = optionalLastOrderId.get();
            }

            criteriaQuery.select(orderRoot);
            criteriaQuery.where(criteriaBuilder.greaterThan(orderRoot.get("id"), lastOrderIdFromReport));
            criteriaQuery.orderBy(criteriaBuilder.desc(orderRoot.get("id")));

            Query<Order> query = session.createQuery(criteriaQuery);

            long count = query.stream().count();
            model.addAttribute("ordersCount", count);
            long pages = 0;
            if (count != limit) {
                pages = Math.round(count / (double) limit);
            }
            model.addAttribute("pages", pages);

            query.setFirstResult(offset);
            query.setMaxResults(limit);

            resultList = query.getResultList();
        }
        return resultList;
    }

    /**
     * Get last offset from session, default offset is zero.
     *
     * @param offset  offset
     * @param session user session
     * @return last known offset
     */
    private Integer setOffsetFromSession(Integer offset, HttpSession session) {
        if (offset == null) {
            Object lastOffset = session.getAttribute("lastOffset");
            if (lastOffset != null) {
                offset = (Integer) lastOffset;
            } else {
                offset = 0;
            }
        }
        session.setAttribute("lastOffset", offset);
        return offset;
    }

    /**
     * Delete order from database.
     *
     * @param id id of order
     * @return url for redirecting
     */
    @DeleteMapping("/{id}")
    public String deleteOrder(@PathVariable int id) {
        seniorCashierService.deleteOrder(id);
        return "redirect:/seniorcashier";
    }

    /**
     * Delete goods in some order.
     *
     * @param orderId order id
     * @param goodsId goods id in that order
     * @return url for redirecting
     */
    @DeleteMapping("/{orderId}/delete/{goodsId}")
    public String deleteGoodsInOrder(@PathVariable int orderId,
                                     @PathVariable int goodsId) {
        seniorCashierService.deleteGoodsInOrder(orderId, goodsId);
        return "redirect:/seniorcashier";
    }

    @GetMapping("/register-user")
    public String registerUserPage(HttpSession session,
                                   Model model) {
        if (session.getAttribute(SUCCESS_REGISTRATION) != null) {

            model.addAttribute(SUCCESS_REGISTRATION, true);
            model.addAttribute(USER_LOGIN, session.getAttribute(USER_LOGIN));
            model.addAttribute(USER_PASSWORD, session.getAttribute(USER_PASSWORD));
            model.addAttribute(USER_ROLE, session.getAttribute(USER_ROLE));

            session.removeAttribute(SUCCESS_REGISTRATION);
            session.removeAttribute(USER_LOGIN);
            session.removeAttribute(USER_PASSWORD);
            session.removeAttribute(USER_ROLE);

        } else {

            markIfWrongInput(INCORRECT_LOGIN, session, model);
            markIfWrongInput("isLoginUsed", session, model);
            markIfWrongInput("incorrectPassword", session, model);
            markIfWrongInput("incorrectRepeatedPassword", session, model);
            passLoginOnPage(session, model);

        }

        return "seniorcashier/registerUser";
    }

    /**
     * Check if input correct and if so save new user in database. If not show error on page.
     *
     * @param session              user session
     * @param userLogin            new user login
     * @param userPassword         new user password
     * @param repeatedUserPassword new user repeated password
     * @param userRole             new user role in system
     * @return url for redirecting
     */
    @PostMapping("/register-user")
    public String registerNewUser(HttpSession session,
                                  @RequestParam(name = "userLogin") String userLogin,
                                  @RequestParam(name = "userPassword") String userPassword,
                                  @RequestParam(name = "repeatedUserPassword") String repeatedUserPassword,
                                  @RequestParam(name = "userRole") String userRole) {
        boolean isAllInputCorrect = isNewUserInputCorrect(userLogin, userPassword, repeatedUserPassword, session);
        if (isAllInputCorrect) {
            seniorCashierService.registerNewUser(session, userLogin, userPassword, userRole);
        }
        return "redirect:/seniorcashier/register-user";
    }

    /**
     * If username already exist return false. If login do not match regex return false. If password do not match regex
     * or repeated password do not equal other password return false.
     *
     * @param userLogin            new user login
     * @param userPassword         new user password
     * @param repeatedUserPassword new user repeated password
     * @param session              user session
     * @return true or false depending on rules above
     */
    public boolean isNewUserInputCorrect(String userLogin, String userPassword, String repeatedUserPassword, HttpSession session) {
        boolean isInputCorrect = isLoginFree(userLogin, session);
        if (isInputCorrect && isLoginCorrect(userLogin)) {
            session.setAttribute(LOGIN_INPUT, userLogin);
            LOGGER.info("Login correct, login: {}", userLogin);
        } else {
            isInputCorrect = false;
            session.setAttribute(INCORRECT_LOGIN, true);
            LOGGER.error("Cannot register new user, wrong login, or already used: {}", userLogin);
        }
        if (!isPasswordCorrect(userPassword)) {
            isInputCorrect = false;
            session.setAttribute("incorrectPassword", true);
            LOGGER.error("Cannot register new user, wrong password: {}", userPassword);
        }
        if (!userPassword.equals(repeatedUserPassword)) {
            isInputCorrect = false;
            session.setAttribute("incorrectRepeatedPassword", true);
            LOGGER.error("Cannot register new user, password and repeated password are different, password: {}, repeated password: {}", userPassword, repeatedUserPassword);
        }
        return isInputCorrect;
    }

    @GetMapping("download-report")
    public void downloadReport(@RequestParam(name = "reportType") String reportType,
                               HttpServletResponse response,
                               Principal principal) {
        Optional<User> userOptional = userRepository.findUserByLogin(principal.getName());
        if (userOptional.isPresent()) {
            int lastOrderIdFromReport = 0;
            Integer lastOrderIdForReport;
            Optional<Integer> optionalLastOrderId = reportRepository.getLastOrderIdFromReport();
            if (optionalLastOrderId.isPresent()) {
                lastOrderIdFromReport = optionalLastOrderId.get();
            }

            if (reportType.equals("Z")) {
                lastOrderIdForReport = orderRepository.getLastOrderId();
            } else {
                lastOrderIdForReport = lastOrderIdFromReport;
            }

            List<Order> orders = orderRepository.getAllByIdGreaterThanOrderByIdDesc(lastOrderIdFromReport);
            ByteArrayOutputStream pdfOutputStream = seniorCashierService.createReport(userOptional.get(), reportType, orders, lastOrderIdForReport);

            response.setContentType("application/pdf");
            response.setContentLength(pdfOutputStream.size());

            LocalDate date = LocalDate.now();
            LocalTime time = LocalTime.now();
            String headerKey = "Content-Disposition";
            String headerValue;
            if (reportType.equals("Z")) {
                headerValue = String.format("attachment; filename=\"%s\"", principal.getName() + "_" + date + "_" + time.format(DateTimeFormatter.ofPattern("HH-mm-ss")) + "_z_report.pdf");
            } else {
                headerValue = String.format("attachment; filename=\"%s\"", principal.getName() + "_" + date + "_" + time.format(DateTimeFormatter.ofPattern("HH-mm-ss")) + "_x_report.pdf");
            }
            response.setHeader(headerKey, headerValue);

            try (ServletOutputStream resOutputStream = response.getOutputStream()) {
                pdfOutputStream.writeTo(resOutputStream);
                resOutputStream.flush();
                pdfOutputStream.close();
                LOGGER.info("REPORT CREATED, TYPE: {}, LOGIN: {}", reportType, principal.getName());
            } catch (IOException e) {
                LOGGER.error("IO report creating exception", e);
            }
        }
    }

    /**
     * If session contains information about wrong input method put it in model and remove from session.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void markIfWrongInput(String errorCode, HttpSession session, Model model) {
        Object incorrectInput = session.getAttribute(errorCode);
        if (incorrectInput != null) {
            model.addAttribute(errorCode, incorrectInput);
            session.removeAttribute(errorCode);
        }
    }

    /**
     * If new user login correct but other not, pass in login page that login.
     *
     * @param session user session
     * @param model   model for thymeleaf resolver
     */
    private void passLoginOnPage(HttpSession session, Model model) {
        String userLogin = (String) session.getAttribute(LOGIN_INPUT);
        if (userLogin != null) {
            model.addAttribute(LOGIN_INPUT, userLogin);
            session.removeAttribute(LOGIN_INPUT);
        }
    }

    /**
     * If login does not blank and match regex return true.
     *
     * @param login new user login
     * @return true or false depending on rule above
     */
    boolean isLoginCorrect(String login) {
        return !login.isBlank() && login.matches("[a-z0-9_-]{5,16}");
    }

    /**
     * If password does not blank and match regex return true.
     *
     * @param password new user password
     * @return true or false depending on rule above
     */
    boolean isPasswordCorrect(String password) {
        return !password.isBlank() && password.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}");
    }

    /**
     * Check if new user login is already exist.
     *
     * @param userLogin new user login
     * @param session user session
     * @return true or false depending on rule above
     */
    boolean isLoginFree(String userLogin, HttpSession session) {
        boolean isLoginUsed = userRepository.findUserByLogin(userLogin).isPresent();
        if (isLoginUsed) {
            session.setAttribute("isLoginUsed", true);
            LOGGER.error("Cannot register new user, login is exist: {}", userLogin);
            return false;
        }
        return true;
    }
}
