package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.entity.Goods;
import com.epam.elearn.cashregister.repository.GoodsRepository;
import com.epam.elearn.cashregister.services.StorekeeperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/storekeeper")
public class StorekeeperController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorekeeperController.class);

    private final GoodsRepository goodsRepository;
    private final StorekeeperService storekeeperService;

    @Autowired
    public StorekeeperController(GoodsRepository goodsRepository, StorekeeperService storekeeperService) {
        this.goodsRepository = goodsRepository;
        this.storekeeperService = storekeeperService;
    }

    @GetMapping
    public String mainPage(@RequestParam(value = "offset", required = false) Integer offset,
                           @RequestParam(value = "limitSelect", required = false) Integer newLimit,
                           HttpSession session,
                           Model model) {
        renderGoodsStorekeeper(offset, newLimit, session, model);
        return "storekeeper/storekeeperMain";
    }

    @GetMapping(value = "/new")
    public String addGoods(@ModelAttribute("goodsObject") Goods goods) {
        return "storekeeper/addNewGoods";
    }

    /**
     * Add new goods. If user input incorrect return anew page with error, else save goods in database.
     *
     * @param goods         goods
     * @param bindingResult user input errors
     * @return anew that page if input incorrect or redirect on main page if input correct
     */
    @PostMapping
    public String addNewGoods(@ModelAttribute("goodsObject") @Valid Goods goods, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            LOGGER.error("Cannot add new goods, info {}", bindingResult);
            return "storekeeper/addNewGoods";
        } else {
            storekeeperService.addNewGoods(goods);
        }
        return "redirect:/storekeeper";
    }

    @GetMapping(value = "/{id}/edit")
    public String changeQuantityPage(Model model, @PathVariable("id") int id) {
        Optional<Goods> optionalGoods = goodsRepository.findById(id);
        optionalGoods.ifPresent(goods -> model.addAttribute("goodsQuantity", goods));
        return "storekeeper/editGoods";
    }

    /**
     * Change guantity of goods.
     *
     * @param goods         goods
     * @param bindingResult errors in user input
     * @param model         model for thymeleaf resolver
     * @return anew that page if input incorrect or redirect on main page if input correct
     */
    @PatchMapping(value = "/{id}")
    public String changeQuantity(@ModelAttribute("goodsQuantity") @Valid Goods goods,
                                 BindingResult bindingResult,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            recognizeQuantityError(bindingResult, model);
            LOGGER.error("Cannot change quantity in goods, info: {}", bindingResult);
            return "storekeeper/editGoods";
        } else {
            storekeeperService.changeQuantity(goods);
        }
        return "redirect:/storekeeper";
    }

    /**
     * Its main method that responsible for pagination and rendering goods on page. Always check limit and current offset
     * for showing items.
     *
     * @param offset   current offset (default zero)
     * @param newLimit if user change limit on page it will be saved here
     * @param session  user session
     * @param model    model for thymeleaf resolver
     */
    private void renderGoodsStorekeeper(Integer offset, Integer newLimit, HttpSession session, Model model) {
        int limit = 5;
        if (newLimit != null) {
            limit = newLimit;
            offset = 0;
            session.setAttribute("lastLimit", limit);
        } else {
            Object lastLimit = session.getAttribute("lastLimit");
            if (lastLimit != null) {
                limit = (Integer) lastLimit;
            }
        }

        offset = setOffsetFromSession(offset, session);

        long pages = 0;
        if (goodsRepository.count() != limit) {
            pages = Math.round(goodsRepository.count() / (double) limit);
        }

        model.addAttribute("goods", goodsRepository.findAllPagination(limit, offset));
        model.addAttribute("pages", pages);
        model.addAttribute("goodsCount", goodsRepository.count());
        model.addAttribute("offset", offset);
        long currentPage = Math.round(offset / (double) limit);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("limit", limit);
    }

    /**
     * Get last offset from session, default offset is zero.
     *
     * @param offset  offset
     * @param session user session
     * @return last known offset
     */
    private Integer setOffsetFromSession(Integer offset, HttpSession session) {
        if (offset == null) {
            Object lastOffset = session.getAttribute("lastOffset");
            if (lastOffset != null) {
                offset = (Integer) lastOffset;
            } else {
                offset = 0;
            }
        }
        session.setAttribute("lastOffset", offset);
        return offset;
    }

    /**
     * Look up that error linked with quantity field, if so show error on page
     *
     * @param bindingResult results of user input
     * @param model         model that show errors on page
     */
    private void recognizeQuantityError(BindingResult bindingResult, Model model) {
        FieldError fieldError = bindingResult.getFieldError("quantity");
        if (fieldError != null) {
            showQuantityErrorOnPage(model, fieldError);
        }
    }

    /**
     * If error linked with quantity field and number was wrote via coma, show coma error on page.
     * If just wrong number, show common error.
     *
     * @param model      model that show errors on page
     * @param fieldError value which input user
     */
    private void showQuantityErrorOnPage(Model model, FieldError fieldError) {
        Object rejectedValue = fieldError.getRejectedValue();
        if (rejectedValue != null) {
            String amount = rejectedValue.toString();
            if (amount.contains(",")) {
                model.addAttribute("comaError", true);
            } else {
                model.addAttribute("error", true);
            }
        }
    }

}
