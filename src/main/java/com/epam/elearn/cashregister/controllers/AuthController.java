package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Optional;

@Controller
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    private static final String LOGIN_REDIRECT = "redirect:/login";

    private final UserRepository userRepository;

    @Autowired
    public AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Show login page. When user was successfully registered show congratulations.
     *
     * @param session user session
     * @param model   thymeleaf model
     * @return login page
     */
    @GetMapping("/login")
    public String loginPage(HttpSession session, Model model) {
        Object currentlyRegistered = session.getAttribute("currentlyRegistered");
        if (currentlyRegistered != null) {
            model.addAttribute("youRegistered", true);
            session.removeAttribute("currentlyRegistered");
        }
        return "login";
    }

    /**
     * Method that redirect users after authorization. If someone else without authorization lets try
     * and open this url, they will be redirected on login page. If somehow user would not have correct role,
     * they will be also redirected on login page. If username in 'principal' not exist in database, user will be redirected
     * on login page. In other contexts' user will be redirected on related page.
     *
     * @param session   user session, save username in it
     * @param principal simple entity from spring security that contain username
     * @return url for redirecting
     */
    @GetMapping("/login/redirect")
    public String redirectRolePage(HttpSession session, Principal principal) {
        if (principal == null) {
            return LOGIN_REDIRECT;
        }
        Optional<User> user = userRepository.findUserByLogin(principal.getName());
        String role;
        if (user.isPresent()) {
            role = user.get().getRole();
            session.setAttribute("username", principal.getName());
        } else {
            LOGGER.error("Cannot redirect user to right role (repository have not user), login: {}", principal.getName());
            return LOGIN_REDIRECT;
        }
        switch (role) {
            case "CASHIER":
                return "redirect:/cashier";
            case "STOREKEEPER":
                return "redirect:/storekeeper";
            case "SENIORCASHIER":
                return "redirect:/seniorcashier";
            default:
                return LOGIN_REDIRECT;
        }
    }

}
