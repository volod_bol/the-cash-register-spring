package com.epam.elearn.cashregister.auth;

import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * It's service that looking users for Spring Security in database. If user not fount then throw UsernameNotFoundException.
 * Users in system have only roles, so SimpleGrantedAuthority contain only user role.
 */
@Service
public class ApplicationUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationUserDetailsService.class);

    private final UserRepository userRepository;

    @Autowired
    public ApplicationUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findUserByLogin(login).orElseThrow(() -> {
            LOGGER.error(String.format("Can not find user, login: %s", login));
            return new UsernameNotFoundException("Cannot find user, login: " + login);
        });
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + user.getRole());
        return new ApplicationUserDetails(user.getLogin(), user.getPassword(), Set.of(authority));
    }

}
