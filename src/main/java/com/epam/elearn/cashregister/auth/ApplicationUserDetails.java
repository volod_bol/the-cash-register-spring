package com.epam.elearn.cashregister.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

/**
 * It's class that Spring Security uses for containing information about authenticated users.
 * System hasn't functionality for ban and lock users, so methods that control it always return the same responses.
 */
public class ApplicationUserDetails implements UserDetails {

    private final String login;
    private final String password;
    private final Set<? extends GrantedAuthority> authorities;

    public ApplicationUserDetails(String login, String password, Set<? extends GrantedAuthority> authorities) {
        this.login = login;
        this.password = password;
        this.authorities = authorities;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
