package com.epam.elearn.cashregister.entity;

/**
 * Enum of users' roles in system
 */
public enum Role {
    CASHIER, STOREKEEPER, SENIORCASHIER
}
