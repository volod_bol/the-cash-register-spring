package com.epam.elearn.cashregister.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Entity of goods that was ordered, contain ordered goods and amount in check.
 * Order use list of that objects when cashier add new goods to receipt.
 */
@Entity
@Table(name = "order_goods")
public class OrderGoods implements Serializable {

    private static final long serialVersionUID = 2021243512288239990L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    private Goods goods;

    @Column(name = "amount")
    private double amount;

    public OrderGoods() {
    }

    public OrderGoods(Goods goods, double amount) {
        this.goods = goods;
        this.amount = amount;
    }

    public OrderGoods(int id, Goods goods, double amount) {
        this.id = id;
        this.goods = goods;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "OrderGoods{" +
                "id=" + id +
                ", goodsId=" + goods.getId() +
                ", goodsTitle='" + goods.getTitle() + "'" +
                ", amount=" + amount +
                '}';
    }
}
