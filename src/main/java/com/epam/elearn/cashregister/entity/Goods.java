package com.epam.elearn.cashregister.entity;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.util.Objects;

/**
 * Entity of goods from warehouse, has id, title and quantity
 */
@Entity
@Table(name = "goods")
public class Goods implements Serializable {

    private static final long serialVersionUID = 2041243512249239990L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Length(message = "Довжина має бути меньше 255", max = 255)
    @NotBlank(message = "Потрібно ввести назву")
    @Column(name = "title", unique = true)
    private String title;

    @Digits(integer = 8, fraction = 3, message = "Неправильна кількість")
    @Positive(message = "Кількість має буде більше 0")
    @Column(name = "quantity", nullable = false)
    private double quantity;

    public Goods() {
    }

    public Goods(String title, double quantity) {
        this.title = title;
        this.quantity = quantity;
    }

    public Goods(int id, String title, double quantity) {
        this.id = id;
        this.title = title;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goods goods = (Goods) o;
        return id == goods.id && Double.compare(goods.quantity, quantity) == 0 && title.equals(goods.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, quantity);
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
