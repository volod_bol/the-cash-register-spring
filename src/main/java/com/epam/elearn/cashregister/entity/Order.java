package com.epam.elearn.cashregister.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * Entity of order. Order contain information about user who create order,
 * date and time of creating and list of ordered goods (id, title and amount).
 */
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    private User user;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "time")
    private LocalTime time;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OrderGoods> orderGoodsList;

    public Order() {
    }

    public Order(User user, LocalDate date, LocalTime time, List<OrderGoods> orderGoodsList) {
        this.user = user;
        this.date = date;
        this.time = time;
        this.orderGoodsList = orderGoodsList;
    }

    public Order(int id, User user, LocalDate date, LocalTime time, List<OrderGoods> orderGoodsList) {
        this.id = id;
        this.user = user;
        this.date = date;
        this.time = time;
        this.orderGoodsList = orderGoodsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public List<OrderGoods> getOrderGoodsList() {
        return orderGoodsList;
    }

    public void setOrderGoodsList(List<OrderGoods> orderGoodsList) {
        this.orderGoodsList = orderGoodsList;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", user=" + user +
                ", date=" + date +
                ", time=" + time +
                ", orderGoodsList=" + orderGoodsList +
                '}';
    }
}
