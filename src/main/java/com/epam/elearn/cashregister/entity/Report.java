package com.epam.elearn.cashregister.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Entity of report. Save information about user, date, time and type of report.
 * The main thing is last order id during creating report. Orders showed for senior cashier
 * only if their id greater than last order id in Z report. Z report it`s report of new trade day.
 */
@Entity
@Table(name = "report")
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "last_order_id")
    @NotNull
    private long lastOrderId;

    @Column(name = "type")
    @NotNull
    private String type;

    @OneToOne
    @NotNull
    private User user;

    @Column(name = "date")
    @NotNull
    private LocalDate date;

    @Column(name = "time")
    @NotNull
    private LocalTime time;

    public Report() {
    }

    public Report(long lastOrderId, String type, User user, LocalDate date, LocalTime time) {
        this.lastOrderId = lastOrderId;
        this.type = type;
        this.user = user;
        this.date = date;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getLastOrderId() {
        return lastOrderId;
    }

    public void setLastOrderId(int lastOrderId) {
        this.lastOrderId = lastOrderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Report report = (Report) o;
        return id == report.id && lastOrderId == report.lastOrderId && type.equals(report.type) && user.equals(report.user) && date.equals(report.date) && time.equals(report.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastOrderId, type, user, date, time);
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", lastOrderId=" + lastOrderId +
                ", type='" + type + '\'' +
                ", date=" + date +
                ", time=" + time +
                '}';
    }
}
