package com.epam.elearn.cashregister.repository;

import com.epam.elearn.cashregister.entity.Report;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ReportRepository extends CrudRepository<Report, Integer> {
    @Query(value = "SELECT last_order_id FROM report WHERE type = 'Z' ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Optional<Integer> getLastOrderIdFromReport();
}