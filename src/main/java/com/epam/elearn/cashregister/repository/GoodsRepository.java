package com.epam.elearn.cashregister.repository;

import com.epam.elearn.cashregister.entity.Goods;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GoodsRepository extends CrudRepository<Goods, Integer> {
    @Query(value = "SELECT * FROM goods ORDER BY title LIMIT ?1 OFFSET ?2", nativeQuery = true)
    List<Goods> findAllPagination(int limit, int offset);

    Optional<Goods> findByTitle(String title);
}
