package com.epam.elearn.cashregister.repository;

import com.epam.elearn.cashregister.entity.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Integer> {

    long countOrdersByIdGreaterThan(int lastOrderId);

    List<Order> getAllByIdGreaterThanOrderByIdDesc(int lastOrderId);

    @Query(value = "SELECT id FROM orders ORDER BY id DESC LIMIT 1", nativeQuery = true)
    Integer getLastOrderId();
}
