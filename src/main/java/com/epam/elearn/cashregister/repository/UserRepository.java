package com.epam.elearn.cashregister.repository;

import com.epam.elearn.cashregister.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    Optional<User> findUserByLogin(String login);

}
