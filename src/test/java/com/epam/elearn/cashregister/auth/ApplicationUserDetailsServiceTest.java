package com.epam.elearn.cashregister.auth;

import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ApplicationUserDetailsServiceTest {

    private static UserRepository repository;
    private static ApplicationUserDetailsService service;

    @BeforeAll
    static void objMocking() {
        repository = mock(UserRepository.class);
        service = new ApplicationUserDetailsService(repository);
    }

    @Test
    void user_not_found() {
        when(repository.findUserByLogin(anyString())).thenReturn(Optional.empty());
        assertThrows(UsernameNotFoundException.class, () -> service.loadUserByUsername("userLogin"));
    }

    @Test
    void user_exist() {
        User user = new User(23, "valentina", "hfdydfydf", "CASHIER");
        when(repository.findUserByLogin(anyString())).thenReturn(Optional.of(user));
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + user.getRole());
        UserDetails userDetails = service.loadUserByUsername("valentina");

        assertEquals(Set.of(authority), userDetails.getAuthorities());
        assertEquals(user.getLogin(), userDetails.getUsername());
        assertEquals(user.getPassword(), userDetails.getPassword());
    }

}