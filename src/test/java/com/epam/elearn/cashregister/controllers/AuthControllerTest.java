package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.entity.Role;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpSession;

import java.security.Principal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AuthControllerTest {

    private static UserRepository repository;
    private static AuthController controller;

    @BeforeAll
    static void objMocking() {
        repository = mock(UserRepository.class);
        controller = new AuthController(repository);
    }

    @Disabled
    void get_mapping_return_right_page() {
//        assertEquals("login", controller.loginPage());
    }

    @Test
    void null_principal() {
        HttpSession session = mock(HttpSession.class);

        assertEquals("redirect:/login", controller.redirectRolePage(session, null));
    }

    @Test
    void no_user_redirect_test() {
        HttpSession session = mock(HttpSession.class);
        Principal principal = mock(Principal.class);
        Optional<User> optionalUser = Optional.empty();

        when(principal.getName()).thenReturn("volod");
        when(repository.findUserByLogin("volod")).thenReturn(optionalUser);

        assertEquals("redirect:/login", controller.redirectRolePage(session, principal));
    }

    @Test
    void wrong_role_redirect_test() {
        HttpSession session = mock(HttpSession.class);
        Principal principal = mock(Principal.class);
        User user = mock(User.class);
        Optional<User> optionalUser = Optional.of(user);

        when(principal.getName()).thenReturn("volod");
        when(repository.findUserByLogin("volod")).thenReturn(optionalUser);
        when(user.getRole()).thenReturn("ADMIN");

        assertEquals("redirect:/login", controller.redirectRolePage(session, principal));
    }

    @Test
    void cashier_redirect_test() {
        HttpSession session = mock(HttpSession.class);
        Principal principal = mock(Principal.class);
        User user = mock(User.class);
        Optional<User> optionalUser = Optional.of(user);

        when(principal.getName()).thenReturn("volod");
        when(repository.findUserByLogin("volod")).thenReturn(optionalUser);
        when(user.getRole()).thenReturn(Role.CASHIER.name());

        assertEquals("redirect:/cashier", controller.redirectRolePage(session, principal));
    }

    @Test
    void storekeeper_redirect_test() {
        HttpSession session = mock(HttpSession.class);
        Principal principal = mock(Principal.class);
        User user = mock(User.class);
        Optional<User> optionalUser = Optional.of(user);

        when(principal.getName()).thenReturn("volod");
        when(repository.findUserByLogin("volod")).thenReturn(optionalUser);
        when(user.getRole()).thenReturn(Role.STOREKEEPER.name());

        assertEquals("redirect:/storekeeper", controller.redirectRolePage(session, principal));
    }

    @Test
    void senior_redirect_test() {
        HttpSession session = mock(HttpSession.class);
        Principal principal = mock(Principal.class);
        User user = mock(User.class);
        Optional<User> optionalUser = Optional.of(user);

        when(principal.getName()).thenReturn("volod");
        when(repository.findUserByLogin("volod")).thenReturn(optionalUser);
        when(user.getRole()).thenReturn(Role.SENIORCASHIER.name());

        assertEquals("redirect:/seniorcashier", controller.redirectRolePage(session, principal));
    }

}