package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.entity.Role;
import com.epam.elearn.cashregister.entity.User;
import com.epam.elearn.cashregister.repository.OrderRepository;
import com.epam.elearn.cashregister.repository.ReportRepository;
import com.epam.elearn.cashregister.repository.UserRepository;
import com.epam.elearn.cashregister.services.SeniorCashierService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpSession;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class SeniorCashierControllerTest {

    private SeniorCashierController seniorCashierController;
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        OrderRepository orderRepository = mock(OrderRepository.class);
        userRepository = mock(UserRepository.class);
        ReportRepository reportRepository = mock(ReportRepository.class);
        seniorCashierController = new SeniorCashierController(
                orderRepository,
                userRepository,
                reportRepository,
                mock(EntityManagerFactory.class),
                new SeniorCashierService(reportRepository, orderRepository, mock(PasswordEncoder.class), userRepository)
        );
    }

    /**
     * Login must be greater than / equal to 5 characters and less than 16 characters, must contain only Latin letters.
     * The use of _- and numbers is allowed
     *
     * @param l login
     */
    @ParameterizedTest
    @ValueSource(strings = {"volod", "marianna_yung", "kolosovsky3", "valentina46", "kravets-anna", "volodymyr"})
    void correct_login(String l) {
        assertTrue(seniorCashierController.isLoginCorrect(l));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "  \n  ", "volo56d^", "marianna*yung", "kolosovsky.", "valent!na+", "kravets=anna", "volodymyr?"})
    void incorrect_login(String l) {
        assertFalse(seniorCashierController.isLoginCorrect(l));
    }

    /**
     * Password must be at least 6 and at most 12 characters, contain only Latin letters,
     * at least one uppercase letter and a number
     *
     * @param p password
     */
    @ParameterizedTest
    @ValueSource(strings = {"bfg5Hds", "dgs66GTD", "xcnGG24", "gFjsk77", "483652fF", "xs77W4"})
    void correct_password(String p) {
        assertTrue(seniorCashierController.isPasswordCorrect(p));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "dgsD", "\t", "$^*$#^@)#_%", "trteetlfF", "xsWNCB%S&*#"})
    void incorrect_password(String p) {
        assertFalse(seniorCashierController.isPasswordCorrect(p));
    }

    @ParameterizedTest
    @ValueSource(strings = {"volod", "marianna_yung", "kolosovsky3", "valentina46", "kravets-anna", "volodymyr"})
    void login_already_used(String login) {
        HttpSession session = mock(HttpSession.class);
        when(userRepository.findUserByLogin(login)).thenReturn(Optional.of(new User(login, "password", Role.CASHIER.name())));

        assertFalse(seniorCashierController.isLoginFree(login, session));

        verify(session).setAttribute("isLoginUsed", true);
    }

    @ParameterizedTest
    @ValueSource(strings = {"volod", "marianna_yung", "kolosovsky3", "valentina46", "kravets-anna", "volodymyr"})
    void login_free(String login) {
        HttpSession session = mock(HttpSession.class);
        when(userRepository.findUserByLogin(login)).thenReturn(Optional.empty());

        assertTrue(seniorCashierController.isLoginFree(login, session));
    }

    @ParameterizedTest
    @CsvSource({
            "volo56d^,dgsD,fdjgfk",
            "marianna*yung,$^*$#^@)#_%,dgsD",
            "volod,xcnGG24,trteetlfF"
    })
    void input_data_of_new_user_incorrect(String userLogin, String userPassword, String repeatedUserPassword) {
        HttpSession session = mock(HttpSession.class);

        assertFalse(seniorCashierController.isNewUserInputCorrect(userLogin, userPassword, repeatedUserPassword, session));
    }

    @ParameterizedTest
    @CsvSource({
            "volod,xcnGG24,xcnGG24",
            "kolosovsky3,483652fF,483652fF",
            "valentina46,xs77W4,xs77W4"
    })
    void input_data_of_new_user_correct(String userLogin, String userPassword, String repeatedUserPassword) {
        HttpSession session = mock(HttpSession.class);

        assertTrue(seniorCashierController.isNewUserInputCorrect(userLogin, userPassword, repeatedUserPassword, session));
    }

}