package com.epam.elearn.cashregister.controllers;

import com.epam.elearn.cashregister.repository.GoodsRepository;
import com.epam.elearn.cashregister.services.CashierService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class CashierControllerTest {

    private CashierController cashierController;

    @BeforeEach
    void setUp() {
        cashierController = new CashierController(mock(GoodsRepository.class), mock(CashierService.class));
    }

    /**
     * Id must be integer
     *
     * @param id identifier
     */
    @ParameterizedTest
    @ValueSource(strings = {"14.6", "-5", "-543.6", "fgfd", "/.d", "64,3", "   ", ""})
    void order_with_wrong_id(String id) {
        assertFalse(cashierController.isOrderInputCorrect("id", id, "25"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"14", "1", "6", "63425334", "7456", "11"})
    void order_with_right_id(String id) {
        assertTrue(cashierController.isOrderInputCorrect("id", id, "25"));
    }

    @Test
    void order_with_null_id() {
        assertFalse(cashierController.isOrderInputCorrect("id", null, "25"));
    }

    /**
     * Amount must be greater than 0 and wrote through dot or coma
     *
     * @param amount goods amount in check
     */
    @ParameterizedTest
    @ValueSource(strings = {"-3.35", "00", "0", "-24,132", "fgddgd", ".teret", "test", "   ", ""})
    void order_with_wrong_amount(String amount) {
        assertFalse(cashierController.isOrderInputCorrect("title", "1", amount));
    }

    @ParameterizedTest
    @ValueSource(strings = {"14,4627", "1.545", "0.48830", "63425334", "0533.5", "4535.2"})
    void order_with_right_amount(String amount) {
        assertTrue(cashierController.isOrderInputCorrect("title", "1", amount));
    }

    /**
     * The same rules as for {@link CashierControllerTest#order_with_wrong_amount(String amount)}
     *
     * @param n number
     */
    @ParameterizedTest
    @ValueSource(strings = {"-53", "0", "-0", "-8344.24", "fsgdgd", "    ", ""})
    void incorrect_number(String n) {
        assertFalse(cashierController.isQuantityCorrect(n));
    }

    @ParameterizedTest
    @ValueSource(strings = {"53", "0.3643", "0,5334", "8344.24", "769353", "23.6464646878", "74"})
    void correct_number(String n) {
        assertTrue(cashierController.isQuantityCorrect(n));
    }

}