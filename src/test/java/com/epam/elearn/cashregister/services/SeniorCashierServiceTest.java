package com.epam.elearn.cashregister.services;

import com.epam.elearn.cashregister.entity.*;
import com.epam.elearn.cashregister.repository.OrderRepository;
import com.epam.elearn.cashregister.repository.ReportRepository;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class SeniorCashierServiceTest {

    private SeniorCashierService service;
    private OrderRepository orderRepository;
    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        ReportRepository reportRepository = mock(ReportRepository.class);
        orderRepository = mock(OrderRepository.class);
        passwordEncoder = mock(PasswordEncoder.class);
        userRepository = mock(UserRepository.class);
        service = new SeniorCashierService(reportRepository, orderRepository, passwordEncoder, userRepository);
    }

    @Test
    void delete_order_that_exist() {
        User user = new User(23, "valentina", "kfkdkfdkkfd", Role.CASHIER.name());
        Optional<Order> optionalOrder = Optional.of(new Order(234, user, LocalDate.now(), LocalTime.now(), new ArrayList<>()));
        when(orderRepository.findById(anyInt())).thenReturn(optionalOrder);

        service.deleteOrder(234);

        verify(orderRepository).delete(optionalOrder.get());
    }

    @Disabled
    void delete_order_that_not_exist() {
        when(orderRepository.findById(anyInt())).thenReturn(Optional.empty());

        service.deleteOrder(234);
    }

    @Test
    void delete_goods_from_order_that_exist() {
        User user = new User(23, "valentina", "kfkdkfdkkfd", Role.CASHIER.name());
        List<OrderGoods> orderGoodsList = List.of(
                new OrderGoods(5, new Goods(23, "XBOX", 64), 1),
                new OrderGoods(6, new Goods(34, "Macbook M1 Ultra", 25), 1),
                new OrderGoods(7, new Goods(14, "LG Monitor", 100), 2)
        );
        Order order = new Order(234, user, LocalDate.now(), LocalTime.now(), orderGoodsList);
        Optional<Order> optionalOrder = Optional.of(order);
        when(orderRepository.findById(anyInt())).thenReturn(optionalOrder);

        service.deleteGoodsInOrder(234, 6);

        verify(orderRepository).save(order);
    }

    @Test
    void delete_goods_from_order_that_not_exist() {
        Optional<Order> optionalOrder = Optional.empty();
        when(orderRepository.findById(anyInt())).thenReturn(optionalOrder);

        service.deleteGoodsInOrder(234, 9);

        verify(orderRepository, never()).save(any(Order.class));
    }

    @ParameterizedTest
    @CsvSource({
            "kolosok,hfb67Gds,SENIORCASHIER",
            "fdgs,hfb67Gds,STOREKEEPER",
            "kolosok,hfb67,CASHIER",
            "kolosok,hfb67Gds,CASHIER",
            "kolosok,hfb67Gds,SENIORCASHIER"
    })
    void register_new_user(String userLogin, String userPassword, String userRole) {
        HttpSession session = mock(HttpSession.class);

        when(passwordEncoder.encode(anyString())).thenReturn("$2a$10$ooMJ10d5y8Hv4gG.fg1Wfun6m.q9A.bZtSU5G78PgKNsVcW1PTuh6");

        service.registerNewUser(
                session,
                userLogin,
                userPassword,
                userRole);

        User user = new User(userLogin, "$2a$10$ooMJ10d5y8Hv4gG.fg1Wfun6m.q9A.bZtSU5G78PgKNsVcW1PTuh6", userRole);
        verify(userRepository).save(user);
        verify(session).setAttribute("successRegistration", true);
        verify(session).setAttribute("userLogin", userLogin);
        verify(session).setAttribute("userPassword", userPassword);
        verify(session).setAttribute("userRole", userRole);
    }

}