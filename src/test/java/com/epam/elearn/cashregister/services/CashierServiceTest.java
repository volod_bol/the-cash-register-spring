package com.epam.elearn.cashregister.services;

import com.epam.elearn.cashregister.entity.*;
import com.epam.elearn.cashregister.repository.GoodsRepository;
import com.epam.elearn.cashregister.repository.OrderRepository;
import com.epam.elearn.cashregister.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class CashierServiceTest {

    private static CashierService service;
    private static GoodsRepository goodsRepository;
    private static OrderRepository orderRepository;
    private static UserRepository userRepository;

    @BeforeAll
    static void objMocking() {
        goodsRepository = mock(GoodsRepository.class);
        orderRepository = mock(OrderRepository.class);
        userRepository = mock(UserRepository.class);
        service = new CashierService(goodsRepository, orderRepository, userRepository);
    }

    /**
     * When user add in check the item, need check if check already has it. If so, method just sum their amount.
     */
    @Test
    void new_item_in_order() {
        List<OrderGoods> orderList = new ArrayList<>();
        OrderGoods oldItem = mock(OrderGoods.class);
        OrderGoods newItem = mock(OrderGoods.class);
        orderList.add(oldItem);

        Goods oldGoods = mock(Goods.class);
        Goods newGoods = mock(Goods.class);
        when(oldItem.getGoods()).thenReturn(oldGoods);
        when(newItem.getGoods()).thenReturn(newGoods);

        when(oldGoods.getTitle()).thenReturn("str");
        when(newGoods.getTitle()).thenReturn("str1");

        service.addGoodsToList(orderList, newItem);

        assertEquals(2, orderList.size());
    }

    @Test
    void item_already_in_order() {
        List<OrderGoods> orderList = new ArrayList<>();
        OrderGoods oldItem = mock(OrderGoods.class);
        OrderGoods newItem = mock(OrderGoods.class);
        orderList.add(oldItem);

        Goods oldGoods = mock(Goods.class);
        Goods newGoods = mock(Goods.class);
        when(oldItem.getGoods()).thenReturn(oldGoods);
        when(newItem.getGoods()).thenReturn(newGoods);

        when(oldGoods.getTitle()).thenReturn("str");
        when(newGoods.getTitle()).thenReturn("str");

        when(oldItem.getAmount()).thenReturn(25.5);
        when(newItem.getAmount()).thenReturn(4.6);

        service.addGoodsToList(orderList, newItem);

        verify(oldItem).setAmount(25.5 + 4.6);
    }

    @Test
    void add_goods_to_order_by_id_goods_exist_new_item() {
        String goodsIdentifierInput = "7";
        String goodsAmount = "26";
        List<OrderGoods> orderGoodsList = new ArrayList<>();
        HttpSession session = mock(HttpSession.class);

        when(session.getAttribute("orderGoodsList")).thenReturn(orderGoodsList);
        Goods goods = new Goods(7, "Baunti", 40);
        Optional<Goods> goodsOptional = Optional.of(goods);
        when(goodsRepository.findById(anyInt())).thenReturn(goodsOptional);

        service.addGoodsToOrderById(goodsIdentifierInput, goodsAmount, session);

        verify(session).setAttribute(eq("lastAddedGoods"), any(OrderGoods.class));
    }

    @Test
    void add_goods_to_order_by_id_goods_non_exist() {
        String goodsIdentifierInput = "7";
        String goodsAmount = "26";
        List<OrderGoods> orderGoodsList = new ArrayList<>();
        HttpSession session = mock(HttpSession.class);

        when(session.getAttribute("orderGoodsList")).thenReturn(orderGoodsList);
        Optional<Goods> goodsOptional = Optional.empty();
        when(goodsRepository.findById(anyInt())).thenReturn(goodsOptional);

        service.addGoodsToOrderById(goodsIdentifierInput, goodsAmount, session);

        verify(session).setAttribute("incorrectOrderInput", true);
    }

    @Test
    void add_goods_to_order_by_title_goods_exist_new_item() {
        String goodsIdentifierInput = "Baunti";
        String goodsAmount = "26";
        List<OrderGoods> orderGoodsList = new ArrayList<>();
        HttpSession session = mock(HttpSession.class);

        when(session.getAttribute("orderGoodsList")).thenReturn(orderGoodsList);
        Goods goods = new Goods(7, "Baunti", 40);
        Optional<Goods> goodsOptional = Optional.of(goods);
        when(goodsRepository.findByTitle(anyString())).thenReturn(goodsOptional);

        service.addGoodsToOrderByTitle(goodsIdentifierInput, goodsAmount, session);

        verify(session).setAttribute(eq("lastAddedGoods"), any(OrderGoods.class));
    }

    @Test
    void add_goods_to_order_by_title_goods_non_exist() {
        String goodsIdentifierInput = "Baunti";
        String goodsAmount = "26";
        List<OrderGoods> orderGoodsList = new ArrayList<>();
        HttpSession session = mock(HttpSession.class);

        when(session.getAttribute("orderGoodsList")).thenReturn(orderGoodsList);
        Optional<Goods> goodsOptional = Optional.empty();
        when(goodsRepository.findByTitle(anyString())).thenReturn(goodsOptional);

        service.addGoodsToOrderByTitle(goodsIdentifierInput, goodsAmount, session);

        verify(session).setAttribute("incorrectOrderInput", true);
    }

    @Test
    void confirm_order() {
        List<OrderGoods> orderGoodsList = new ArrayList<>();
        orderGoodsList.add(new OrderGoods(new Goods(64, "Monitor DELL", 55), 3));
        Principal principal = mock(Principal.class);
        HttpSession session = mock(HttpSession.class);

        User user = new User(32, "volod", "dfkkfkdkf", Role.SENIORCASHIER.name());
        Optional<User> optionalUser = Optional.of(user);
        when(principal.getName()).thenReturn("volod");
        when(userRepository.findUserByLogin("volod")).thenReturn(optionalUser);

        service.confirmOrder(orderGoodsList, session, principal);

        verify(orderRepository).save(any(Order.class));
        verify(session).removeAttribute("orderGoodsList");
    }

}