# the-cash-register-spring

There are roles: cashier, senior cashier, storekeeper.

The storekeeper can create goods and indicate their quantity in the warehouse.

The cashier has the opportunity to:
- create an order (check);
- add selected products by their code or by product name to the order;
- specify / change the quantity of a certain product or weight;
- close the order (check);

The senior cashier has the opportunity to:
- cancel the check;
- cancel the goods in the receipt;
- make X and Z reports.
***
Technologies/Frameworks:
-
* Java 11
* Spring Boot (Web, Security, Data JPA)
* JUnit
* Mockito
* Maven
* Bootstrap
* Thymeleaf
* Spring Resource Bundle (i18n, i10n)
* Log4j2
* RDBMS: PostgreSQL
***
Features
-
* i18n
* Pagination
* Authentication and authorization
* Delimitation of authorities
* All inputs with data validation
* Error behavior (user don`t see server stack trace)
***
Participation:
-
- Developed whole application logic using Spring Boot and Spring dependencies (Spring Web, Spring Data JPA, Spring Security)
- Configured user authentication and authorization by Spring Security
- Created and configured a custom login page (the application doesn't use the default login page from Spring Security)
- Implemented SQL Transactions methods for a procession and confirming orders
- Formed secure credentials in a database (password encrypted by Spring Security BCryptPasswordEncoder)
- Designed HTML pages with Thymeleaf for displaying information from the database (using loops and other features from the template engine)
- Separated access to pages for each user role by Spring Security
- Configured full warning/error logging by Log4j2
- Configured i18n (Ukrainian, English) by Spring Resource Bundle (language changing provided by own written JavaScript function)
- Designed all HTML pages using Bootstrap 5 for a maximum user-friendly view
- Involved algorithm for pagination without using additional frameworks (application get the only necessary information for page from database)
- Configured exception handling (user doesn't see server stack trace)
- Provided fields validation on the backend side
- Covered whole business logic and 50% of project code by tests

***
Database diagram:
-
![db](spring_cashregister-db.png)
***
First run
-
Before first run change line in src/main/resources/application.properties from

`spring.jpa.hibernate.ddl-auto=update`
to
`spring.jpa.hibernate.ddl-auto=create`

After launch change that back.

Change db connect information:
```
spring.datasource.url=jdbc:postgresql://localhost:5432/cashregisterjpa
spring.datasource.username=postgres
spring.datasource.password=root
```
In src/main/resources/log4j2.xml change APP_LOG_ROOT path for logs
```
<Properties>
    <Property name="LOG_PATTERN">%d{yyyy-MM-dd 'T'HH:mm:ss.SSSZ} %p %m%n</Property>
    <Property name="APP_LOG_ROOT">C:\Users\volod\Desktop\theCashRegister\cashregister\logs
    </Property>
</Properties>
```
When tables were created, insert data from sql/db-insert.sql (basic users and goods for warehouse).
